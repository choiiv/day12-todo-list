import {createBrowserRouter } from 'react-router-dom';
import TodoList from '../components/TodoList';
import AboutPage from '../pages/AboutPage';
import DoneList from '../components/DoneList';
import Layout from '../layout/Layout';
import TodoDetail from '../components/TodoDetail';
import NonFoundPage from '../pages/NotFoundPage';

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        errorElement: <NonFoundPage />,
        children: [
            {
                index: true,
                element: <TodoList />,
            },
            {
                path: "done",
                index: true,
                element: <DoneList />,
            },
            {
                path: "about",
                index: true,
                element: <AboutPage />,
            },
            {
                path: "todos/:id",
                index: true,
                element: <TodoDetail />,
            },
        ]
    },
  ]);

export default router;