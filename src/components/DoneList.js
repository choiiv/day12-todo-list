import { useSelector } from "react-redux";
import DoneItem from "./DoneItem";

const DoneList = () => {
    const doneItems = useSelector((state) => state.toDoList.toDoItems)
                        .filter((item) => item.done);
    return (
        <div>
            <h1 className="text-3xl font-bold mb-10">DoneList</h1>
            {doneItems
                .map((item) => {
                    return <DoneItem key={item.id} item={item} />
                })
            }
        </div>
    )
}

export default DoneList;