import { useTodos } from "./hooks/useTodos";
import { EditOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { useEffect, useState } from 'react';

const TodoItem = (props) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalText, setModalText] = useState("");

    const {item} = props;
    const {toggleTodo, deleteTodo} = useTodos();
    const {updateTodoText} = useTodos();

    useEffect(() => {
        setModalText(item.text);
    }, [])

    const handleFinishTodo = () => {
        toggleTodo(item);
    }

    const handleRemoveTodoItem = () => {
        if(window.confirm("Are you sure to delete the item?")){
            deleteTodo(item);
        }
    }

    const handleTextChange = (event) => {
        setModalText(event.target.value);
    }

    const showModal = () => {
        setIsModalOpen(true);
    }

    const handleOk = () => {
        updateTodoText(item, modalText);
        setIsModalOpen(false);
    }

    const handleCancel = () => {
        setModalText(item.text);
        setIsModalOpen(false);
    }

    const textStyle = {
        textDecoration: item.done ? "line-through" : "none"
    }

    return (
        <div className="flex rounded-md border-2 border-gray-300 my-1 justify-between">
            <div className="pl-4 flex-1 text-left" style={textStyle} onClick={handleFinishTodo}>{item.text}</div>
            <div className="px-2 flex-none" onClick={handleRemoveTodoItem}>x</div>
            <EditOutlined className="px-2 flex-none" onClick={showModal} />
            <Modal title="TodoItem" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}
                okButtonProps={{
                    className: 'bg-gray-100 text-black',
                    disabled: modalText.trim().length === 0,
                }}
            >
                <textarea className="w-full border-2 rounded border-gray-300" 
                    name="todo-item"
                    type="text"
                    value={modalText}
                    onChange={handleTextChange}/>
            </Modal>
        </div>
    );
}

export default TodoItem;