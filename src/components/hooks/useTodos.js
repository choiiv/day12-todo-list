import { createTodo, getTodos, updateTodo, removeTodo } from "../../api/todos"
import { useDispatch } from "react-redux";
import { setTodoItems } from "../../app/toDoSlice";

export const useTodos = () => {
    const dispatch = useDispatch();

    const loadTodos = () => {
        getTodos().then((response) => {
            dispatch(setTodoItems(response.data));
        })
    }

    const addTodo = async (todoItem) => {
        await createTodo(todoItem);
        loadTodos();
    }

    const toggleTodo = async (item) => {
        const newItem = {...item, done: !item.done};
        await updateTodo(newItem);
        loadTodos();
    }

    const updateTodoText = async (item, text) => {
        const newItem = {...item, text: text};
        await updateTodo(newItem);
        loadTodos();
    }

    const deleteTodo = async (item) => {
        await removeTodo(item.id);
        loadTodos();
    }

    return {
        loadTodos,
        addTodo,
        toggleTodo,
        deleteTodo,
        updateTodoText,
    }
}