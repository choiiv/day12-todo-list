import { useState } from "react";
import { useTodos } from "./hooks/useTodos";
import { Button } from 'antd';


const TodoGenerator = () => {
    const [text, setText] = useState("");
    const {addTodo} = useTodos();

    const generateHandler = () => {
        if(text.trim() === "") {
            return;
        }

        const newToDoItem = {
            text: text,
            done: false,
        }

        addTodo(newToDoItem);
    }

    const handleTextChange = (event) => {
        setText(event.target.value);
    }

    return (
        <div className="flex w-1/2 mx-auto justify-center">
            <input className="border-2 rounded border-gray-300" name="todo-item" type="text" placeholder="input a new todo here..." onChange={handleTextChange}/>
            <Button onClick={generateHandler}>add</Button>
        </div>
    );
}

export default TodoGenerator;