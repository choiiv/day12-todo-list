import { Link } from "react-router-dom";

const DoneItem = (props) => {
    const {item} = props;

    return (
        <div className="w-1/2 mx-auto rounded-md border-2 border-gray-300 my-1 justify-between">
            <Link to={"/todos/" + item.id}>
                <div className="pl-4 flex-1 text-left">{item.text}</div>
            </Link>
        </div>
    );
}

export default DoneItem;