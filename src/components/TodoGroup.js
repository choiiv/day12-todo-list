import { useSelector } from "react-redux";
import TodoItem from "./TodoItem";
import { useEffect } from "react";
import { getTodos } from "../api/todos";
import { useDispatch } from 'react-redux';
import { setTodoItems } from "../app/toDoSlice";

const TodoGroup = () => {
    const toDoItems = useSelector((state) => state.toDoList.toDoItems);
    const dispatch = useDispatch();
    useEffect(() => {
        getTodos().then((response) => {
            // dispatch data
            dispatch(setTodoItems(response.data))
            console.log(response);
        });
    }, [])

    return (
        <div className="flex flex-col w-1/2 mx-auto mb-20">
            {toDoItems.map((toDoItem) => (
                <TodoItem key={toDoItem.id} item={toDoItem}/>
            ))}
        </div>
    );
}

export default TodoGroup;