import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";

const TodoList = () => {
    return (
        <div>
            <h1 className="text-3xl font-bold py-10">TodoList</h1>
            <TodoGroup />
            <TodoGenerator />
        </div>
    );
}

export default TodoList;