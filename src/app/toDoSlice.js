import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    toDoItems: []
}

const toDoSlice = createSlice({
    name: "ToDoList",
    initialState,
    reducers: {
        addTodoItem(state, action) {
            state.toDoItems = [...state.toDoItems, action.payload];
        },

        toggleItemDone(state, action) {
            const index = state.toDoItems
                            .findIndex((item) => item.id === action.payload);
            state.toDoItems[index].done = !state.toDoItems[index].done;
        },

        removeToDoItem(state, action) {
            state.toDoItems = state.toDoItems.filter((item) => {
                return item.id !== action.payload;
            });
        },

        setTodoItems(state, action) {
            state.toDoItems = action.payload;
        },
    }
})

export const {addTodoItem, toggleItemDone, removeToDoItem, setTodoItems} = toDoSlice.actions;
export default toDoSlice.reducer