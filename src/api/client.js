import axios from "axios";

const client = axios.create({
    baseURL: "https://6566e0fa64fcff8d730f3397.mockapi.io"
})

export default client;