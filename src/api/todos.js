import client from "./client"

export const getTodos = () => {
    return client.get("/todos");
}

export const createTodo = (todoItem) => {
    return client.post("/todos", todoItem);
}

export const updateTodo = (todoItem) => {
    return client.put("/todos/" + todoItem.id, todoItem);
}

export const removeTodo = (id) => {
    return client.delete("/todos/" + id);
}