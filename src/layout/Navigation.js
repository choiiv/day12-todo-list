import { Link } from "react-router-dom";
import "./Navigation.css"
import { useState } from "react";

const Navigation = () => {
    const tabItemCSS = "text-center block border border-blue-500 rounded py-2 px-4 text-white transition delay-100 duration-300 ease-in-out hover:bg-blue-700 transform hover:scale-110 "

    return (
        <nav className="bg-blue-500 py-2 pr-4">
            <ul className="flex space-x-4">
                <div class="flex-1"></div>
                <li class="flex-none">
                    <Link className={tabItemCSS} to="/">
                        Home
                    </Link>
                </li>
                <li class="flex-none">
                    <Link className={tabItemCSS} to="/done">
                        Done
                    </Link>
                </li>
                <li class="flex-none">
                    <Link className={tabItemCSS} to="/about">
                        About
                    </Link>
                </li>
            </ul>
        </nav>
    );
}

export default Navigation;